using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] Vector3 _movementSpeed = new Vector3(5, 0, 0);

    // Update is called once per frame
    void Update()
    {
        var position = transform.position;
        position += _movementSpeed * Time.deltaTime;
        transform.position = position;
    }
}
