using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
        Debug.Log($"{gameObject.name} triggered by {col.gameObject.name}");
        if (col.gameObject.CompareTag("Player"))
        {
            var position = col.gameObject.transform.position;
            position.x = -position.x;
            col.gameObject.transform.position = position;
        }
    }
}
