using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log($"{gameObject.name} collided with {col.gameObject.name}");
    }
}
